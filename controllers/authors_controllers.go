package controllers

import (
	"apiauthors/configs"
	"apiauthors/responses"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
)

type AuthorsAll struct {
	Authors []Authorsdetail `json:"authors"`
	Details []Details       `json:"details"`
}

type Authorsdetail struct {
	Author_id        int            `json:"author_id"`
	Author_name      string         `json:"author_name"`
	Author_code      string         `json:"author_code"`
	Author_type_id   int            `json:"author_type_id"`
	Active_status    int            `json:"author_status"`
	Date_modified    string         `json:"date_modified"`
	Institution_id   int            `json:"institution_id"`
	Updater_id       int            `json:"updater_id"`
	Images           sql.NullString `json:"images"`
	Summary          sql.NullString `json:"summary"`
	Updater_name     sql.NullString `json:"updater_name"`
	Institution_name sql.NullString `json:"institution_name"`
	Author_type      string         `json:"author_type"`
}

type Details struct {
	Id        int    `json:"id"`
	Socname   string `json:"socname"`
	Soclink   string `json:"soclink"`
	Author_id int    `json:"author_id"`
	Created   string `json:"created"`
	Updated   string `json:"updated"`
}

type detailfruit struct {
	Buah string
	Rasa string
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
func GetData(c echo.Context) error {
	var idauthors = c.Param("idauthors")
	dat, err := os.ReadFile("/mainData/data/xml/authors/" + idauthors + ".json")
	if dat != nil {
		check(err)
		bytes := []byte(dat)
		//fmt.Print(string(bytes))

		var authorstr AuthorsAll
		json.Unmarshal(bytes, &authorstr)

		return c.JSON(http.StatusOK, responses.AuthorsResponse{Status: http.StatusOK, Message: "Data found", Data: &echo.Map{"item": authorstr}})
	} else {

		dbs, err := configs.MyConnected()

		sql := "SELECT a.author_id,a.author_name,a.author_code,a.author_type_id,a.active_status, a.date_modified, a.institution_id, a.summary,a.images,a.updater_id,b.user_name as updater_name,i.institution_name,t.author_type FROM td_author as a LEFT JOIN td_user b ON a.updater_id=b.user_id LEFT JOIN td_institution as i ON a.institution_id=i.institution_id LEFT JOIN td_author_type as t ON a.author_type_id=t.author_type_id WHERE a.author_id = " + idauthors
		resul, err := dbs.Query(sql)
		if err != nil {
			return err
		}
		var authors Authorsdetail
		for resul.Next() {

			err = resul.Scan(&authors.Author_id, &authors.Author_name, &authors.Author_code, &authors.Author_type_id, &authors.Active_status, &authors.Date_modified, &authors.Institution_id, &authors.Summary, &authors.Images, &authors.Updater_id, &authors.Updater_name, &authors.Institution_name, &authors.Author_type)
			if err != nil {
				panic(err)
			}

		}
		if err != nil {
			panic(err)
		}
		//fmt.Println(authors)

		//==== detail data

		sqldetail := "SELECT id,socname,soclink,author_id,created,updated FROM td_author_detail WHERE author_id=" + idauthors
		resuldetail, err := dbs.Query(sqldetail)
		if err != nil {
			return err
		}
		var details Details
		for resul.Next() {
			if details.Socname != "" {
				err = resuldetail.Scan(&details.Id, &details.Socname, &details.Soclink, &details.Author_id, &details.Created, &details.Updated)
			} else {
				err = resul.Scan()
			}

		}
		//---setup array
		//_ = `{"authors": [{"author_id": 0,"author_name": " Ahmad Husein Lubis (Sindo TV)","author_code": "ahd","author_type_id": 0,"author_status": 0,"date_modified": "2023-03-27 11:07:08","institution_id": 0,"updater_id": 0,"images": "ahd-oFZwbNKSQ7.jpg","updater_name": "ferro","institution_name": "iNews.id","author_type": "Kontributor"}]}`
		res := AuthorsAll{
			Authors: []Authorsdetail{authors},
			Details: []Details{details},
		}
		//json.Unmarshal([]byte(str), &res)
		//fmt.Println(resul)
		if authors.Author_name != "" {
			//mystr := string([]byte(res))
			//err = os.WriteFile("/mainData/data/xml/authors/"+idauthors+".json", res, 777)
			//panic(err)
		}

		return c.JSON(http.StatusNotFound, responses.AuthorsResponse{Status: http.StatusOK, Message: "File not found!", Data: &echo.Map{"item": res}})
	}

	//s := string(bytes)
	//return c.JSON(http.StatusOK, responses.AuthorsResponse{Status: http.StatusOK, Message: "Respose Basic", Data: &echo.Map{"item": ""}})

}
func GetDataMulti(c echo.Context) error {
	var idauthors = c.Param("idauthors")
	dat, err := os.ReadFile("/mainData/data/xml/authors/" + idauthors + "_m.json")
	if dat != nil {
		check(err)
		bytes := []byte(dat)
		//fmt.Print(string(bytes))

		var authorstr AuthorsAll
		json.Unmarshal(bytes, &authorstr)

		return c.JSON(http.StatusOK, responses.AuthorsResponse{Status: http.StatusOK, Message: "Data", Data: &echo.Map{"item": authorstr}})
	} else {
		fmt.Println("File not found")
	}

	//s := string(bytes)
	return c.JSON(http.StatusOK, responses.AuthorsResponse{Status: http.StatusOK, Message: "Respose Basic", Data: &echo.Map{"item": ""}})

}

func CobaMarshal(c echo.Context) error {
	var jsonBlon = []byte(`[{"authors_id":1,"authors_name":"Ferro"}]`)
	type Joauth struct {
		Authors_id   int
		Authors_name string
	}
	var joath []Joauth
	err := json.Unmarshal(jsonBlon, &joath)
	if err != nil {
		return err
	}
	fmt.Println(joath)
	return c.JSON(http.StatusOK, responses.AuthorsResponse{Status: http.StatusOK, Message: "Test code", Data: &echo.Map{"item": joath}})
}

func CobaUnmarshal(c echo.Context) error {
	type Response2 struct {
		Fruits []detailfruit `json:"fruits"`
	}

	str := `{"fruits": [{"buah":"Nangka","rasa":"Manis"}]}`
	res := Response2{}
	json.Unmarshal([]byte(str), &res)
	fmt.Println(res)
	fmt.Println(res.Fruits[0])

	return c.JSON(http.StatusOK, responses.AuthorsResponse{Status: http.StatusOK, Message: "Test code Unmarshal", Data: &echo.Map{"item": res}})
}

func Querynya(c echo.Context) error {

	var idauthors = c.Param("idauthors")
	dbs, err := configs.MyConnected()

	sql := "SELECT a.author_id,a.author_name,a.author_code,a.author_type_id,a.active_status, a.date_modified, a.institution_id, a.summary,a.images,a.updater_id,b.user_name as updater_name,i.institution_name,t.author_type FROM td_author as a LEFT JOIN td_user b ON a.updater_id=b.user_id LEFT JOIN td_institution as i ON a.institution_id=i.institution_id LEFT JOIN td_author_type as t ON a.author_type_id=t.author_type_id WHERE a.author_id = " + idauthors
	resul, err := dbs.Query(sql)
	if err != nil {
		return err
	}
	var authors Authorsdetail
	for resul.Next() {

		err = resul.Scan(&authors.Author_id, &authors.Author_name, &authors.Author_code, &authors.Author_type_id, &authors.Active_status, &authors.Date_modified, &authors.Institution_id, &authors.Summary, &authors.Images, &authors.Updater_id, &authors.Updater_name, &authors.Institution_name, &authors.Author_type)
		if err != nil {
			panic(err)
		}

	}
	if err != nil {
		panic(err)
	}
	fmt.Println(authors)

	//==== detail data

	sqldetail := "SELECT id,socname,soclink,author_id,created,updated FROM td_author_detail WHERE author_id=" + idauthors
	resuldetail, err := dbs.Query(sqldetail)
	if err != nil {
		return err
	}
	var details Details
	for resul.Next() {
		if details.Socname != "" {
			err = resuldetail.Scan(&details.Id, &details.Socname, &details.Soclink, &details.Author_id, &details.Created, &details.Updated)
		} else {
			err = resul.Scan()
		}

	}
	//---setup array
	_ = `{"authors": [{"author_id": 0,"author_name": " Ahmad Husein Lubis (Sindo TV)","author_code": "ahd","author_type_id": 0,"author_status": 0,"date_modified": "2023-03-27 11:07:08","institution_id": 0,"updater_id": 0,"images": "ahd-oFZwbNKSQ7.jpg","updater_name": "ferro","institution_name": "iNews.id","author_type": "Kontributor"}]}`
	res := AuthorsAll{
		Authors: []Authorsdetail{authors},
		Details: []Details{details},
	}
	//json.Unmarshal([]byte(str), &res)
	//fmt.Println(resul)
	if authors.Author_name != "" {
		//mystr := string([]byte(res))
		//err = os.WriteFile("/mainData/data/xml/authors/"+idauthors+".json", res, 777)
		//panic(err)
	}

	return c.JSON(http.StatusOK, responses.AuthorsResponse{Status: http.StatusOK, Message: "Test code Unmarshal", Data: &echo.Map{"item": res}})
}

func GetJson(sqlString string, taskID string) (string, error) {

	dbs, err := configs.MyConnected()
	if err != nil {
		panic(err)
	}

	defer dbs.Close()
	rows, err := dbs.Query(sqlString, taskID)
	if err != nil {
		return "", err
	}

	columns, err := rows.Columns()
	if err != nil {
		return "", err
	}

	count := len(columns)
	tableData := make([]map[string]interface{}, 0)
	values := make([]interface{}, count)
	valuePtrs := make([]interface{}, count)

	for rows.Next() {
		for i := 0; i < count; i++ {
			valuePtrs[i] = &values[i]
		}
		rows.Scan(valuePtrs...)
		entry := make(map[string]interface{})
		for i, col := range columns {
			var v interface{}
			val := values[i]
			b, ok := val.([]byte)
			if ok {
				v = string(b)
			} else {
				v = val
			}
			entry[col] = v
		}
		tableData = append(tableData, entry)
	}
	jsonData, err := json.Marshal(tableData)
	if err != nil {
		return "", err
	}

	return string(jsonData), nil

}
