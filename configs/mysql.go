package configs

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

func MyConnected() (dbmysql *sql.DB, err error) {
	dbDriver := "mysql"
	dbUser := "cms"
	dbPassword := "oke-cms"
	dbName := "oke-cms"
	dbmysql, err = sql.Open(dbDriver, dbUser+":"+dbPassword+"@tcp(172.17.6.200)/"+dbName)
	return
}
