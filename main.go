package main

import (
	"apiauthors/routes"

	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()
	routes.AuthorsRoutes(e)
	e.Logger.Fatal(e.Start(":2021"))
}
