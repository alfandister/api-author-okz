package routes

import (
	"apiauthors/controllers"

	"github.com/labstack/echo/v4"
)

func AuthorsRoutes(e *echo.Echo) {
	e.GET("/authors/:idauthors", controllers.GetData)
	e.GET("/mauthors/:idauthors", controllers.GetDataMulti)
	e.GET("/mauthors", controllers.GetDataMulti)
	e.GET("/textcoba", controllers.CobaMarshal)
	e.GET("/textcobaun", controllers.CobaUnmarshal)
	e.GET("/querynya/:idauthors", controllers.Querynya)
}
